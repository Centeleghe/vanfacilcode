package br.edu.ifrs.simioni.controller;

import java.util.List;
import java.util.Set;

import br.edu.ifrs.simioni.Exception.EmailAlreadyExistsException;
import br.edu.ifrs.simioni.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import br.edu.ifrs.simioni.service.EmpresaService;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {
	
	@Autowired
	private EmpresaService empresaService;
	
	@PostMapping
	public Empresa insertEmpresa(@RequestBody Empresa empresa) {
		return empresaService.postEmpresa(empresa);
	}
	
	@GetMapping
	public List<Empresa> getAllEmpresa() {
		return empresaService.getAllEmpresa();
	}
	
	@GetMapping("/{id}")
	public Empresa getOneEmpresa(@PathVariable("id") Integer id) {
		return empresaService.getOneEmpresa(id);
	}

	@PostMapping("/byUsuario")
	public Empresa getByUsuario(@RequestBody Usuario usuario){
		return empresaService.getByUsuario(usuario);
	}
	
	@DeleteMapping(path="/{id}")
	public void deleteEmpresa(@PathVariable("id") Integer id) {
		empresaService.deleteEmpresa(id);
	}

	@GetMapping("/allRotas/{id}")
	public Set<Rota> getRotasEmpresa(@PathVariable("id") Integer id) {return empresaService.getRotasEmpresa(id);}

	@GetMapping("/allPassageiros/{id}")
	public Set<Passageiro> getPassageirosEmpresa(@PathVariable("id") Integer id) {return empresaService.getPassageirosEmpresa(id);}

	@GetMapping("/allMotoristas/{id}")
	public Set<Motorista> getMotoristasEmpresa(@PathVariable("id") Integer id) {return empresaService.getMotoristasEmpresa(id);}
}
