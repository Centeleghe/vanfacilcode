package br.edu.ifrs.simioni.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.ifrs.simioni.model.Usuario;
import br.edu.ifrs.simioni.service.UsuarioService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@PostMapping
	public Usuario insertUsuario(@RequestBody Usuario usuario) {
		return usuarioService.postUsuario(usuario);
	}
	
	@GetMapping
	public List<Usuario> getAllUsuario(){
		return usuarioService.getAllUsuario();
	}
	
	@GetMapping(path="/{login}")
	public Usuario getOneUsuario(@PathVariable("login") Integer login) {
		return usuarioService.getOneUsuario(login);
	}

	@DeleteMapping(path="/{login}")
	public void deleteUsuario(@PathVariable("login") Integer login) {
		usuarioService.deleteUsuario(login);
	}

	@PostMapping(value = "/auth")
	public Usuario usuarioAuth(@RequestBody Usuario usuario) {
		return usuarioService.usuarioAuth(usuario);
	}
}
