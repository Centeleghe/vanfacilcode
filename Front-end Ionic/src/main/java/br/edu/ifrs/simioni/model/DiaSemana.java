package br.edu.ifrs.simioni.model;

public enum DiaSemana {
    SEGUNDA, TERCA, QUARTA, QUINTA, SEXTA;
}
