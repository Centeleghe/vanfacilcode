package br.edu.ifrs.simioni.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@Table(name="motorista_rota")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MotoristaRota {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_motorista_rota")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_motorista")
    @JsonIgnoreProperties({"usuario", "motoristaRotaSet"})
    private Motorista motorista;

    @ManyToOne
    @JoinColumn(name = "id_rota")
    @JsonIgnoreProperties({"motoristaRotaSet"})
    private Rota rota;

    @Override
    public String toString() {
        return "MotoristaRota{" +
                "id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MotoristaRota)) return false;
        MotoristaRota that = (MotoristaRota) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
