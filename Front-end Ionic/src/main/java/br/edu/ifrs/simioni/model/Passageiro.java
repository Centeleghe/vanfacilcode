package br.edu.ifrs.simioni.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@Table(name="passageiro")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Passageiro {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="id_usuario", referencedColumnName = "id")
    private Usuario usuario;

    @ManyToOne
    @JsonIgnoreProperties({"usuario", "rotas", "passageiros", "motoritas"})
    private Empresa empresa;

    @JsonIgnore
    @OneToMany(mappedBy = "passageiro", cascade = CascadeType.ALL, orphanRemoval = true)
    Set<PassageiroRota> passageiroRotaSet;

    @Column(name = "nome")
    private String nome;

    @Column(name = "contato")
    private Long contato;

    @Column(name = "responsavel")
    private String responsavel;

    @Column(name = "endereco")
    private String endereco;

    private Double lat;

    private Double lng;
}
