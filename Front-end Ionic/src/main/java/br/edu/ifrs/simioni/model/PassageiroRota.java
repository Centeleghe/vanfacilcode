package br.edu.ifrs.simioni.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@Table(name="passageiro_rota")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PassageiroRota {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_passageiro_rota")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_passageiro")
    @JsonIgnoreProperties({"usuario", "passageiroRotaSet"})
    private Passageiro passageiro;

    @ManyToOne
    @JoinColumn(name = "id_rota")
    @JsonIgnoreProperties({"passageiroRotaSet"})
    private Rota rota;

    @Column(name = "valor_pago")
    private Long valorPago;

    @OneToMany(mappedBy = "passageiroRota", cascade = CascadeType.ALL)
    private Set<Cronograma> cronogramas;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PassageiroRota that = (PassageiroRota) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "PassageiroRota{" +
                "id=" + id +
                '}';
    }
}
