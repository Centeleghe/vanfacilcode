package br.edu.ifrs.simioni.model;

public enum TipoViagem {
    IDA, VOLTA;
}
