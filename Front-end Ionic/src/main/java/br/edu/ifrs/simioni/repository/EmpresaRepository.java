package br.edu.ifrs.simioni.repository;

import br.edu.ifrs.simioni.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.ifrs.simioni.model.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Integer>{
    Empresa getByUsuario(Usuario usuario);
}
