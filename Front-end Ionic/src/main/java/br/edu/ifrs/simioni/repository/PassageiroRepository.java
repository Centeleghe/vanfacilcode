package br.edu.ifrs.simioni.repository;

import br.edu.ifrs.simioni.model.Passageiro;
import br.edu.ifrs.simioni.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassageiroRepository extends JpaRepository<Passageiro, Integer> {
    Passageiro getByUsuario(Usuario usuario);
}
