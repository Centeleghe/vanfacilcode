package br.edu.ifrs.simioni.roterizador.domains;

import br.edu.ifrs.simioni.roterizador.domains.Visita;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Atividade {
    private Integer ordem;
    private Visita visita;

    private Atividade(Builder builder) {
        this.ordem = builder.ordem;
        this.visita = builder.visita;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Integer ordem;
        private Visita visita;

        private Builder() {
        }

        public Builder ordem(Integer ordem) {
            this.ordem = ordem;
            return this;
        }

        public Builder visita(Visita visita) {
            this.visita = visita;
            return this;
        }

        public Atividade build() {
            return new Atividade(this);
        }
    }
}
