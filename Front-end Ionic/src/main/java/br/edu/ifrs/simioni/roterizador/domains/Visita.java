package br.edu.ifrs.simioni.roterizador.domains;

import br.edu.ifrs.simioni.model.Passageiro;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
public class Visita {
    private String id;
    private Coordenada coordenada;
    private Passageiro passageiro;

    public static Visita forPassageiro(Passageiro passageiro) {
        return new Visita(String.valueOf(passageiro.getId()),
                new Coordenada(passageiro.getLat(), passageiro.getLng()),
                passageiro);
    }
}
