package br.edu.ifrs.simioni.roterizador.solucionador.Calculadora;

import br.edu.ifrs.simioni.roterizador.Services.DistanceMatrixService;
import br.edu.ifrs.simioni.roterizador.domains.DistanciaTempo;
import br.edu.ifrs.simioni.roterizador.domains.SolicitacaoRoteirizacao;
import com.graphhopper.jsprit.core.problem.Location;
import com.graphhopper.jsprit.core.problem.cost.AbstractForwardVehicleRoutingTransportCosts;
import com.graphhopper.jsprit.core.problem.vehicle.Vehicle;
import org.apache.commons.math3.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Driver;
import java.util.HashMap;

public class CalculadoraGeralCustoTempoTransporte extends AbstractForwardVehicleRoutingTransportCosts {
    private static final double DEFAULT_SPEED = 11; // m/s

    private HashMap<Pair<String, String>, DistanciaTempo> hashMap;

    public CalculadoraGeralCustoTempoTransporte(HashMap<Pair<String, String>, DistanciaTempo> hashMap) {
        this.hashMap = hashMap;
    }

    @Override
    public double getDistance(Location from, Location to, double departureTime, Vehicle vehicle) {
        return calculateDistance(from, to);
    }

    @Override
    public double getTransportTime(Location from, Location to, double departureTime, com.graphhopper.jsprit.core.problem.driver.Driver driver, Vehicle vehicle) {
        return calculateTime(from, to);
    }

    @Override
    public double getTransportCost(Location from, Location to, double departureTime, com.graphhopper.jsprit.core.problem.driver.Driver driver, Vehicle vehicle) {
        return calculateTime(from, to);
    }

    private  double calculateDistance(Location fromLocation, Location toLocation) {
        if(!(fromLocation.getId().equals(toLocation.getId()))) {
            DistanciaTempo distanciaTempo = hashMap.get(new Pair<>(fromLocation.getId(), toLocation.getId()));
            return distanciaTempo.getDistancia();
        } else {
            return 0;
        }
    }
    private double calculateTime(Location fromLocation, Location toLocation) {
        if(!(fromLocation.getId().equals(toLocation.getId()))) {
            DistanciaTempo distanciaTempo = hashMap.get(new Pair<>(fromLocation.getId(), toLocation.getId()));
            return distanciaTempo.getTempo();
        } else {
            return 0;
        }
    }
}
