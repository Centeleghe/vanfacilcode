package br.edu.ifrs.simioni.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN,
        reason = "Erro de autenticação")
public class AuthFailedException extends RuntimeException {
    public AuthFailedException() {
        super("Erro de autenticação");
    }
}
