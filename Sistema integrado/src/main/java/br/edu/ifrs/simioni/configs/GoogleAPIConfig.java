package br.edu.ifrs.simioni.configs;

import com.google.maps.GeoApiContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GoogleAPIConfig {
    @Bean
    public GeoApiContext generateGeoApiContext() {
        return new GeoApiContext.Builder()
                .apiKey("AIzaSyD99BijxRCyuFMxtXBFWxTBUjjMmBrORW4")
                .build();
    }
}
