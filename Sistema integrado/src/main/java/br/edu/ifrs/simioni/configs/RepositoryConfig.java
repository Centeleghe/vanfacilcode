package br.edu.ifrs.simioni.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;


import br.edu.ifrs.simioni.model.Empresa;
import br.edu.ifrs.simioni.model.Usuario;

@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration restConfig) {
        restConfig.exposeIdsFor(Usuario.class);
        restConfig.exposeIdsFor(Empresa.class);
    }
}
