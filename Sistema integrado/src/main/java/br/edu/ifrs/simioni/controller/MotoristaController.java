package br.edu.ifrs.simioni.controller;

import br.edu.ifrs.simioni.model.Motorista;
import br.edu.ifrs.simioni.model.Rota;
import br.edu.ifrs.simioni.model.Usuario;
import br.edu.ifrs.simioni.service.MotoristaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/motorista")
public class MotoristaController {

    @Autowired
    MotoristaService motoristaService;

    @PostMapping
    public Motorista insertMotorista(@RequestBody Motorista motorista) {
        return motoristaService.postMotorista(motorista);
    }

    @PostMapping("/byUsuario")
    public Motorista getByUsuario(@RequestBody Usuario usuario){
        return motoristaService.getByUsuario(usuario);
    }

    @GetMapping("/rota-by-motorista/{id}")
    public List<Rota> getRotasByMotorista(@PathVariable("id") Integer id) {
        return motoristaService.getRotaByMotorista(id);
    }

    @GetMapping
    public List<Motorista> getAllMotorista() {
        return motoristaService.getAllMotorista();
    }

    @GetMapping("/{id}")
    public Motorista getOneMotorista(@PathVariable("id") Integer id) {
        return motoristaService.getOneMotorista(id);
    }

    @DeleteMapping(path="/{id}")
    public void deleteMotorista(@PathVariable("id") Integer id) {
        motoristaService.deleteMotorista(id);
    }
}

