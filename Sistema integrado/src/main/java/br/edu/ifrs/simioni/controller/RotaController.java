package br.edu.ifrs.simioni.controller;

import br.edu.ifrs.simioni.model.*;
import br.edu.ifrs.simioni.roterizador.domains.Coordenada;
import br.edu.ifrs.simioni.service.RotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/rota")
public class RotaController {
    @Autowired
    RotaService rotaService;

    @PostMapping
    public Rota insertRota(@RequestBody Rota rota) {
        return rotaService.postRota(rota);
    }

    @GetMapping
    public List<Rota> getAllRota() {
        return rotaService.getAllRota();
    }

    @GetMapping("/{id}")
    public Rota getOneRota(@PathVariable("id") Integer id) {
        return rotaService.getOneRota(id);
    }

    @DeleteMapping(path="/{id}")
    public void deleteRota(@PathVariable("id") Integer id) {
        rotaService.deleteRota(id);
    }

    //PASSAGEIRO
    @GetMapping("/passageiro-rota/{id}")
    public List<Passageiro> getPassageirosByRota(@PathVariable("id") Integer id) {
        return rotaService.getPassageirosByRota(id);
    }

    @GetMapping("/passageiros-sobram/{id}")
    public List<Passageiro> getPassageirosForaRota(@PathVariable("id") Integer id) {
        return rotaService.getPassageirosForaRota(id);
    }

    @PutMapping("/add-passageiro-rota/{id}")
    public void addPassageiroToRota(@PathVariable("id") Integer id, @RequestBody Passageiro passageiro) {
        rotaService.addPassageiroToRota(id, passageiro);
    }

    @PutMapping("/delete-passageiro-rota/{id}")
    public void deletePassageiroRota(@PathVariable("id") Integer id, @RequestBody Passageiro passageiro) {
        rotaService.deletePassageiroFromRota(id, passageiro);
    }

    @PutMapping("/passageiros-ordenados")
    public List<Passageiro> getPassageirosOrdenados(@RequestBody RequisicaoPassageirosOrdenados requisicao) {

        return rotaService.getPassageirosOrdenados(requisicao.getIdRota(), requisicao.getTipoViagem(), requisicao.getDiaSemana(), requisicao.getPosicaoVan());
    }

    //MOTORISTA
    @GetMapping("/motorista-rota/{id}")
    public List<Motorista> getMotoristaByRota(@PathVariable("id") Integer id) {
        return rotaService.getMotoristasByRota(id);
    }

    @GetMapping("/motoristas-sobram/{id}")
    public List<Motorista> getMotoristasForaRota(@PathVariable("id") Integer id) {
        return rotaService.getMotoristasForaRota(id);
    }

    @PutMapping("/add-motorista-rota/{id}")
    public void addMotoristaToRota(@PathVariable("id") Integer id, @RequestBody Motorista motorista) {
        rotaService.addMotoristaToRota(id, motorista);
    }

    @PutMapping("/delete-motorista-rota/{id}")
    public void deleteMotoristaRota(@PathVariable("id") Integer id, @RequestBody Motorista motorista) {
        rotaService.deleteMotoristaFromRota(id, motorista);
    }
}
