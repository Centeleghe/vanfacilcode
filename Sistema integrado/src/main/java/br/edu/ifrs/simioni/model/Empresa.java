package br.edu.ifrs.simioni.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.*;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="empresa")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(
		generator = ObjectIdGenerators.PropertyGenerator.class,
		property = "id")
public class Empresa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_usuario", referencedColumnName = "id")
	private Usuario usuario;

	@Column(name = "nome")
	private String nome;
	
	@Column(name = "razao_social")
	private String razaoSocial;
	
	@Column(name = "cnpj")
	private Long cnpj;

	@Column(name = "contato")
	private Long contato;

	@JsonIgnore
	@OneToMany(mappedBy = "empresa", cascade = CascadeType.ALL)
	private Set<Passageiro> passageiros;

	@JsonIgnore
	@OneToMany(mappedBy = "empresa", cascade = CascadeType.ALL)
	private Set<Motorista> motoristas;

	@JsonIgnore
	@OneToMany(mappedBy = "empresa", cascade = CascadeType.ALL)
	private Set<Rota> rotas;


}
