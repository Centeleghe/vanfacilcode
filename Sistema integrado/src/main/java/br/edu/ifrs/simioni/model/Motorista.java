package br.edu.ifrs.simioni.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@Table(name="motorista")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Motorista {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="id_usuario", referencedColumnName = "id")
    private Usuario usuario;

    @ManyToOne
    @JsonIgnoreProperties({"usuario", "rotas", "passageiros", "motoristas"})
    private Empresa empresa;

    @JsonIgnore
    @OneToMany(mappedBy = "motorista", cascade = CascadeType.ALL, orphanRemoval = true)
    Set<MotoristaRota> motoristaRotaSet;

    @Column(name = "nome")
    private String nome;

    @Column(name = "contato")
    private Long contato;

    @Column(name = "salario")
    private Double salario;

    @Column(name = "cnh")
    private Long cnh;
}
