package br.edu.ifrs.simioni.model;

import br.edu.ifrs.simioni.roterizador.domains.Coordenada;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RequisicaoPassageirosOrdenados {
    private Integer idRota;
    private TipoViagem tipoViagem;
    private DiaSemana diaSemana;
    private Coordenada posicaoVan;
}
