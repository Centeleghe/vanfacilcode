package br.edu.ifrs.simioni.model;

public enum Turno {

    MANHA, TARDE, NOITE;
}
