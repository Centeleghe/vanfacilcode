package br.edu.ifrs.simioni.repository;

import br.edu.ifrs.simioni.model.Motorista;
import br.edu.ifrs.simioni.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MotoristaRepository extends JpaRepository<Motorista, Integer> {
    Motorista getByUsuario(Usuario usuario);
}
