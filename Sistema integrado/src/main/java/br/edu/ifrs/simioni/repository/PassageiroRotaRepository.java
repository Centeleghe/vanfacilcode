package br.edu.ifrs.simioni.repository;

import br.edu.ifrs.simioni.model.PassageiroRota;
import br.edu.ifrs.simioni.model.Rota;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PassageiroRotaRepository extends JpaRepository<PassageiroRota, Integer> {
    List<PassageiroRota> getAllByRota(Rota rota);
}
