package br.edu.ifrs.simioni.roterizador.Services;

import br.edu.ifrs.simioni.roterizador.domains.*;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.LatLng;
import com.google.maps.model.Unit;
import org.apache.commons.math3.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

@Service
public class DistanceMatrixService {

    @Autowired
    private GeoApiContext geoApiContext;

    public HashMap<Pair<String, String>, DistanciaTempo> getHashMapDeDistanciaTempo(SolicitacaoRoteirizacao solicitacaoRoteirizacao) {
        HashMap<Pair<String, String>, DistanciaTempo> hashMap = new HashMap<>();
        List<Localizacao> localizacaos = new ArrayList<>();

        for (Visita visita: solicitacaoRoteirizacao.getVisitas()) {
            localizacaos.add(new Localizacao(visita.getId(), visita.getCoordenada()));
        }

        localizacaos.add(new Localizacao("start",
                solicitacaoRoteirizacao
                        .getAgente()
                        .getCoordenadaInicial()
                )
        );
        localizacaos.add(new Localizacao("end",
                solicitacaoRoteirizacao
                        .getAgente()
                        .getCoordenadaFinal()
                )
        );


        for (Localizacao localizacao1: localizacaos) {
            for (Localizacao localizacao2: localizacaos) {
                if(!(localizacao1.getId().equals(localizacao2.getId()))) {
                    hashMap.put( new Pair<>(localizacao1.getId(), localizacao2.getId()),
                            solicitarDistaciaTempo(localizacao1.getCoordenada(), localizacao2.getCoordenada())
                    );
                }
            }
        }

        return hashMap;
    }

    private DistanciaTempo solicitarDistaciaTempo(Coordenada coordenada1, Coordenada coordenada2) {
        DistanciaTempo distanciaTempo = new DistanciaTempo();

        try {
            DistanceMatrix distanceMatrix = new DistanceMatrixApiRequest(geoApiContext)
                    .origins(new LatLng(coordenada1.getLat(), coordenada1.getLng()))
                    .destinations(new LatLng(coordenada2.getLat(), coordenada2.getLng()))
                    .units(Unit.METRIC)
                    .language("pt-BR")
                    .departureTime(LocalDateTime.now().plusSeconds(5).atZone(ZoneId.of("America/Sao_Paulo")).toInstant())
                    .await();

            distanciaTempo.setDistancia(distanceMatrix.rows[0].elements[0].distance.inMeters);
            distanciaTempo.setTempo(distanceMatrix.rows[0].elements[0].durationInTraffic.inSeconds);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return distanciaTempo;
    }
}
