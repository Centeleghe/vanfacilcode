package br.edu.ifrs.simioni.roterizador.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Coordenada {
    private Double lat;

    private Double lng;
}
