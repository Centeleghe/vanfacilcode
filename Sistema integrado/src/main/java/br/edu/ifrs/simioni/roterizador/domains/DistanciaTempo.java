package br.edu.ifrs.simioni.roterizador.domains;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class DistanciaTempo {
    private Long distancia;
    private Long tempo;
}
