package br.edu.ifrs.simioni.roterizador.domains;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor
public class ResultadoRoteirizacao {
    private Agente agente;
    private List<Atividade> atividades;

    private ResultadoRoteirizacao(Builder builder) {
        this.agente = builder.agente;
        this.atividades = builder.atividades;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Agente agente;
        private List<Atividade> atividades = Collections.emptyList();

        private Builder() {
        }

        public Builder atividades(List<Atividade> atividades) {
            this.atividades = atividades;
            return this;
        }

        public Builder agente(Agente agente) {
            this.agente = agente;
            return this;
        }

        public ResultadoRoteirizacao build() {
            return new ResultadoRoteirizacao(this);
        }
    }
}
