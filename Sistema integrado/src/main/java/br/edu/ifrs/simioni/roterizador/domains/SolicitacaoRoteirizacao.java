package br.edu.ifrs.simioni.roterizador.domains;

import br.edu.ifrs.simioni.model.Rota;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SolicitacaoRoteirizacao {
    private Agente agente;
    private List<Visita> visitas;
    private Rota rota;
}
