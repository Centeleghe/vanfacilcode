package br.edu.ifrs.simioni.roterizador.solucionador;

import br.edu.ifrs.simioni.roterizador.Services.DistanceMatrixService;
import br.edu.ifrs.simioni.roterizador.domains.Agente;
import br.edu.ifrs.simioni.roterizador.domains.DistanciaTempo;
import br.edu.ifrs.simioni.roterizador.domains.SolicitacaoRoteirizacao;
import br.edu.ifrs.simioni.roterizador.domains.Visita;
import br.edu.ifrs.simioni.roterizador.solucionador.Calculadora.CalculadoraGeralCustoTempoTransporte;
import com.graphhopper.jsprit.core.problem.Location;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem;
import com.graphhopper.jsprit.core.problem.job.Delivery;
import com.graphhopper.jsprit.core.problem.job.Service;
import com.graphhopper.jsprit.core.problem.vehicle.Vehicle;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleImpl;
import com.graphhopper.jsprit.core.util.Coordinate;
import org.apache.commons.math3.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class SolicitacaoRoteirizacaoToVrpMapper {
    @Autowired
    DistanceMatrixService distanceMatrixService;

    public VehicleRoutingProblem map(SolicitacaoRoteirizacao solicitacaoRoteirizacao) {

        HashMap<Pair<String, String>, DistanciaTempo> hashMap = distanceMatrixService.getHashMapDeDistanciaTempo(solicitacaoRoteirizacao);
        CalculadoraGeralCustoTempoTransporte calculadoraGeralCustoTempoTransporte = new CalculadoraGeralCustoTempoTransporte(hashMap);

        return VehicleRoutingProblem.Builder.newInstance()
                .addAllJobs(toServices(solicitacaoRoteirizacao.getVisitas()))
                .addVehicle(toVehicle(solicitacaoRoteirizacao.getAgente()))
                .setRoutingCost(calculadoraGeralCustoTempoTransporte)
                .build();
    }

    private List<Service> toServices(List<Visita> visitas){
        return visitas.stream()
                .map(v -> toService(v))
                .collect(Collectors.toList());
    }

    private Service toService(Visita visita) {
        return Delivery.Builder
                .newInstance(visita.getId())
                .setLocation(Location.Builder
                        .newInstance()
                        .setId(visita.getId())
                        .setCoordinate(new Coordinate(
                                visita.getCoordenada().getLat(),
                                visita.getCoordenada().getLng()))
                        .build())
                .setUserData(visita)
                .build();
    }

    private Vehicle toVehicle(Agente agente) {
        return VehicleImpl.Builder.newInstance(agente.getId())
                .setStartLocation(Location.Builder
                        .newInstance()
                        .setId("start")
                        .setCoordinate(new Coordinate(
                                agente.getCoordenadaInicial().getLat(),
                                agente.getCoordenadaInicial().getLng()))
                        .build())
                .setEndLocation(Location.Builder
                        .newInstance()
                        .setId("end")
                        .setCoordinate(new Coordinate(
                                agente.getCoordenadaFinal().getLat(),
                                agente.getCoordenadaFinal().getLng()))
                        .build())
                .setUserData(agente)
                .build();
    }
}
