package br.edu.ifrs.simioni.roterizador.solucionador;

import br.edu.ifrs.simioni.roterizador.domains.Agente;
import br.edu.ifrs.simioni.roterizador.domains.Atividade;
import br.edu.ifrs.simioni.roterizador.domains.ResultadoRoteirizacao;
import br.edu.ifrs.simioni.roterizador.domains.Visita;
import com.graphhopper.jsprit.core.problem.solution.VehicleRoutingProblemSolution;
import com.graphhopper.jsprit.core.problem.solution.route.VehicleRoute;
import com.graphhopper.jsprit.core.problem.solution.route.activity.DeliverService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class VrpToSolucaoRoteirizacaoMapper {

    public ResultadoRoteirizacao map(VehicleRoutingProblemSolution vrpSolution) {
        return ResultadoRoteirizacao.builder()
                .agente(toAgente(vrpSolution.getRoutes()))
                .atividades(toAtividades(vrpSolution.getRoutes()))
                .build();
    }

    private Agente toAgente(Collection<VehicleRoute> routes) {
        return (Agente) routes.iterator()
                .next()
                .getVehicle()
                .getUserData();
    }

    private List<Atividade> toAtividades(Collection<VehicleRoute> routes) {
        List<DeliverService> serviceActivities = routes.iterator()
                .next()
                .getActivities()
                .stream()
                .filter(a -> a instanceof DeliverService)
                .map(a -> (DeliverService) a)
                .collect(Collectors.toList());

        AtomicInteger atomicInteger = new AtomicInteger();

        return serviceActivities.stream()
                .map(j -> toAtividade(j, atomicInteger.incrementAndGet()))
                .collect(Collectors.toList());
    }

    private Atividade toAtividade(DeliverService serviceActivity, int ordemAtividade) {
        Visita visita = (Visita) serviceActivity.getJob().getUserData();

        return Atividade.builder()
                .ordem(ordemAtividade)
                .visita(visita)
                .build();
    }
}
