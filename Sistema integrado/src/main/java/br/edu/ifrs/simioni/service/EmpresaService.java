package br.edu.ifrs.simioni.service;

import java.util.List;
import java.util.Set;

import br.edu.ifrs.simioni.Exception.EmailAlreadyExistsException;
import br.edu.ifrs.simioni.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.ifrs.simioni.repository.EmpresaRepository;

import javax.validation.constraints.Null;

@Service
public class EmpresaService {
	
	@Autowired
	private	EmpresaRepository empresaRepository;
    @Autowired
    private  UsuarioService usuarioService;

	public Empresa postEmpresa(Empresa empresa) throws EmailAlreadyExistsException {

		if(empresa.getId() == null) {
			if (!usuarioService.existsUsuarioByLogin(empresa.getUsuario().getLogin())) {

				Usuario usuarioResponse = usuarioService.postUsuario(empresa.getUsuario());
				empresa.setUsuario(usuarioResponse);

				return empresaRepository.save(empresa);
			} else {
				throw new EmailAlreadyExistsException();
			}
		}else {
			Empresa empresaBanco = this.getOneEmpresa(empresa.getId());
			empresa.setUsuario(empresaBanco.getUsuario());
			Empresa empresaSalva = empresaRepository.save(empresa);
			empresaSalva.setUsuario(null);
			return empresaSalva;
		}
	}

	public Empresa getOneEmpresa(Integer id) {
		return empresaRepository.getOne(id);
	}
	
	public List<Empresa> getAllEmpresa(){
		return empresaRepository.findAll();
	}
	
	public void deleteEmpresa(Integer id) {
		Empresa empresa = this.getOneEmpresa(id);
		empresaRepository.deleteById(id);
		usuarioService.deleteUsuario(empresa.getUsuario().getId());
	}

	public Empresa getByUsuario(Usuario usuario) {
		Empresa empresa = empresaRepository.getByUsuario(usuario);
		empresa.setUsuario(null);
		return empresa;
	}

	public Set<Rota> getRotasEmpresa(Integer id){
		Empresa empresa = this.getOneEmpresa(id);
		return empresa.getRotas();
	}

	public Set<Passageiro> getPassageirosEmpresa(Integer id){
		Empresa empresa = empresaRepository.getOne(id);
		return empresa.getPassageiros();
	}

	public Set<Motorista> getMotoristasEmpresa(Integer id){
		Empresa empresa = empresaRepository.getOne(id);
		return empresa.getMotoristas();
	}

}
