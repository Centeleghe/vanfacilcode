package br.edu.ifrs.simioni.service;

import java.util.List;

import br.edu.ifrs.simioni.Exception.AuthFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.edu.ifrs.simioni.model.Usuario;
import br.edu.ifrs.simioni.repository.UsuarioRepository;

@Service
public class UsuarioService {
	@Autowired
	private UsuarioRepository usuarioRepository;


	public Usuario postUsuario(Usuario usuario) {
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		usuario.setSenha(passwordEncoder.encode(usuario.getSenha()));

		return usuarioRepository.save(usuario);
	}
	
	public Usuario getOneUsuario(Integer id) {
		return usuarioRepository.getOne(id);
	}

	public Usuario getUsuarioByLogin(String login) {
		return usuarioRepository.getByLogin(login);
	}

	public List<Usuario> getAllUsuario(){
		return usuarioRepository.findAll();
	}
	
	public void deleteUsuario(Integer login) {
		usuarioRepository.deleteById(login);
	}

	public Usuario usuarioAuth(Usuario usuario) throws AuthFailedException {
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		if(usuarioRepository.existsUsuarioByLogin(usuario.getLogin())){
			Usuario usuarioBanco = getUsuarioByLogin(usuario.getLogin());

			if(passwordEncoder.matches(usuario.getSenha(), usuarioBanco.getSenha())){
				return usuarioBanco;

			}else {
				throw new AuthFailedException();
			}
		}
		else{
			throw new AuthFailedException();
		}
	}

	public boolean existsUsuarioByLogin(String login) {
		return usuarioRepository.existsUsuarioByLogin(login);
	}
}
