(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cadastro-empresa-cadastro-empresa-module"],{

/***/ "./src/app/cadastro-empresa/cadastro-empresa.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/cadastro-empresa/cadastro-empresa.module.ts ***!
  \*************************************************************/
/*! exports provided: CadastroEmpresaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastroEmpresaPageModule", function() { return CadastroEmpresaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _cadastro_empresa_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cadastro-empresa.page */ "./src/app/cadastro-empresa/cadastro-empresa.page.ts");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "./node_modules/@fortawesome/angular-fontawesome/fesm5/angular-fontawesome.js");








var routes = [
    {
        path: '',
        component: _cadastro_empresa_page__WEBPACK_IMPORTED_MODULE_6__["CadastroEmpresaPage"]
    }
];
var CadastroEmpresaPageModule = /** @class */ (function () {
    function CadastroEmpresaPageModule() {
    }
    CadastroEmpresaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__["FontAwesomeModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_cadastro_empresa_page__WEBPACK_IMPORTED_MODULE_6__["CadastroEmpresaPage"]]
        })
    ], CadastroEmpresaPageModule);
    return CadastroEmpresaPageModule;
}());



/***/ }),

/***/ "./src/app/cadastro-empresa/cadastro-empresa.page.html":
/*!*************************************************************!*\
  !*** ./src/app/cadastro-empresa/cadastro-empresa.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>\n        <fa-icon icon=\"chevron-left\" routerLink=\"/home\" size=\"1x\" pull=\"left\"></fa-icon>\n        Cadastrar empresa\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <form [formGroup]=\"formulario\" (ngSubmit)=\"onSubmit(formulario.value)\">\n      <ion-item>\n        <ion-label position=\"floating\">Nome fictício</ion-label>\n        <ion-input type=\"text\" formControlName=\"nome\"></ion-input>\n      </ion-item>\n\n      <ng-container *ngFor=\"let validation of validationMessages.nome\">\n        <div class=\"error-message\" *ngIf=\"formulario.get('nome').hasError(validation.type) && (formulario.get('nome').dirty || formulario.get('nome').touched)\">\n          <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\n        </div>\n      </ng-container>\n\n      <ion-item>\n        <ion-label position=\"floating\">Razão social</ion-label>\n        <ion-input type=\"text\" formControlName=\"razaoSocial\"></ion-input>\n      </ion-item>\n\n      <ng-container *ngFor=\"let validation of validationMessages.razaoSocial\">\n        <div class=\"error-message\" *ngIf=\"formulario.get('razaoSocial').hasError(validation.type) && (formulario.get('razaoSocial').dirty || formulario.get('razaoSocial').touched)\">\n          <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\n        </div>\n      </ng-container>\n\n      <ion-item>\n        <ion-label position=\"floating\">CNPJ</ion-label>\n        <ion-input type=\"number\" formControlName=\"cnpj\"></ion-input>\n      </ion-item>\n\n      <ng-container *ngFor=\"let validation of validationMessages.cnpj\">\n        <div class=\"error-message\" *ngIf=\"formulario.get('cnpj').hasError(validation.type) && (formulario.get('cnpj').dirty || formulario.get('cnpj').touched)\">\n          <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\n        </div>\n      </ng-container>\n\n      <ion-item>\n        <ion-label position=\"floating\">E-mail</ion-label>\n        <ion-input (keydown)=\"setEmailExistsFalse()\" type=\"email\" formControlName=\"email\"></ion-input>\n      </ion-item>\n\n      <ng-container *ngFor=\"let validation of validationMessages.email\">\n        <div class=\"error-message\" *ngIf=\"formulario.get('email').hasError(validation.type) && (formulario.get('email').dirty || formulario.get('email').touched)\">\n          <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\n        </div>\n      </ng-container>\n\n      <ng-container *ngIf=\"emailExists\">\n          <div class=\"error-message\">\n              <ion-icon name=\"information-circle-outline\"></ion-icon> Este e-mail já foi cadastrado!\n          </div>\n      </ng-container>\n\n      <ion-item>\n        <ion-label position=\"floating\">Contato</ion-label>\n        <ion-input type=\"number\" formControlName=\"contato\"></ion-input>\n      </ion-item>\n\n      <ng-container *ngFor=\"let validation of validationMessages.contato\">\n        <div class=\"error-message\" *ngIf=\"formulario.get('contato').hasError(validation.type) && (formulario.get('contato').dirty || formulario.get('contato').touched)\">\n          <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\n        </div>\n      </ng-container>\n      \n      <ion-item>\n        <ion-label position=\"floating\">Senha</ion-label>\n        <ion-input type=\"password\" formControlName=\"senha\"></ion-input>\n      </ion-item>\n      \n      <ng-container *ngFor=\"let validation of validationMessages.senha\">\n        <div class=\"error-message\" *ngIf=\"formulario.get('senha').hasError(validation.type) && (formulario.get('senha').dirty || formulario.get('senha').touched)\">\n          <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\n        </div>\n      </ng-container>\n        \n      <ion-button color=\"dark\" class=\"enviar-button\" expand=\"full\" type=\"submit\" [disabled]=\"!formulario.valid || !disponibilidade\">Cadastrar</ion-button>\n  </form>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/cadastro-empresa/cadastro-empresa.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/cadastro-empresa/cadastro-empresa.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#map {\n  height: 85%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FkYXN0cm8tZW1wcmVzYS9DOlxcVXNlcnNcXEx1aXMgRWR1YXJkb1xcR2l0XFx2YW5GYWNpbEFwcFYyL3NyY1xcYXBwXFxjYWRhc3Ryby1lbXByZXNhXFxjYWRhc3Ryby1lbXByZXNhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQVcsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NhZGFzdHJvLWVtcHJlc2EvY2FkYXN0cm8tZW1wcmVzYS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbWFwIHtcclxuICBoZWlnaHQ6IDg1JTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/cadastro-empresa/cadastro-empresa.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/cadastro-empresa/cadastro-empresa.page.ts ***!
  \***********************************************************/
/*! exports provided: CadastroEmpresaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastroEmpresaPage", function() { return CadastroEmpresaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _usuario__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../usuario */ "./src/app/usuario.ts");
/* harmony import */ var _tipo_enum__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../tipo.enum */ "./src/app/tipo.enum.ts");
/* harmony import */ var _empresa__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../empresa */ "./src/app/empresa.ts");
/* harmony import */ var _empresa_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../empresa.service */ "./src/app/empresa.service.ts");
/* harmony import */ var _alert_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../alert.service */ "./src/app/alert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");

/* tslint:disable:no-trailing-whitespace */








var CadastroEmpresaPage = /** @class */ (function () {
    function CadastroEmpresaPage(formBuilder, router, empresaService, alertService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.empresaService = empresaService;
        this.alertService = alertService;
        this.validationMessages = {
            nome: [
                { type: 'required', message: 'Este campo é obrigatório' },
                { type: 'maxlength', message: 'Máximo de 100 caractéres' }
            ],
            razaoSocial: [
                { type: 'maxlength', message: 'Máximo de 100 caractéres' }
            ],
            email: [
                { type: 'required', message: 'Campo obrigatório' },
                { type: 'email', message: 'Informe um e-mail válido' },
                { type: 'maxlength', message: 'Máximo de 100 caractéres' }
            ],
            cnpj: [
                { type: 'max', message: 'Máximo de 14 dígitos' }
            ],
            contato: [
                { type: 'max', message: 'Máximo de 14 dígitos' }
            ],
            senha: [
                { type: 'required', message: 'Este campo é obrigatório' },
                { type: 'maxlength', message: 'Máximo de 50 caractéres' }
            ]
        };
        this.disponibilidade = true;
        this.emailExists = false;
        this.formulario = this.formBuilder.group({
            nome: '',
            razao_social: '',
            cnpj: '',
            email: '',
            contato: '',
            senha: ''
        });
    }
    CadastroEmpresaPage.prototype.ngOnInit = function () {
        // document.addEventListener('ionAlertDidDismiss', () => this.redirectPage());
        this.formulario = this.formBuilder.group({
            nome: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(100)
                ]],
            razaoSocial: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(100)
                ]],
            email: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(100)
                ]],
            cnpj: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].max(99999999999999)
                ]],
            contato: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].max(99999999999999)
                ]],
            senha: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)
                ]]
        });
    };
    CadastroEmpresaPage.prototype.onSubmit = function (values) {
        var _this = this;
        this.emailExists = false;
        this.disponibilidade = false;
        var usuario = new _usuario__WEBPACK_IMPORTED_MODULE_3__["Usuario"]();
        usuario.setSenha(values.senha);
        usuario.setTipo(_tipo_enum__WEBPACK_IMPORTED_MODULE_4__["Tipo"].EMPRESA);
        usuario.setLogin(values.email);
        var empresa = new _empresa__WEBPACK_IMPORTED_MODULE_5__["Empresa"]();
        empresa.setNome(values.nome);
        empresa.setRazaoSocial(values.razaoSocial);
        empresa.setCnpj(values.cnpj);
        empresa.setContato(values.contato);
        empresa.setUsuario(usuario);
        this.empresaService.postEmpresa(empresa)
            .subscribe(function (data) { return _this.showAlert(); }, function (error) { return _this.exceptionHandler(error); }, function () { return _this.disponibilidade = true; });
    };
    CadastroEmpresaPage.prototype.showAlert = function () {
        localStorage.setItem('empresaCadastradaAlert', 'true');
        this.redirectPage();
    };
    CadastroEmpresaPage.prototype.redirectPage = function () {
        this.router.navigate(['/home']);
    };
    CadastroEmpresaPage.prototype.setEmailExistsFalse = function () {
        this.emailExists = false;
    };
    CadastroEmpresaPage.prototype.exceptionHandler = function (error) {
        if (error.status === 409) {
            this.emailExists = true;
            this.disponibilidade = true;
        }
        else {
            console.log(error);
        }
    };
    CadastroEmpresaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cadastro-empresa',
            template: __webpack_require__(/*! ./cadastro-empresa.page.html */ "./src/app/cadastro-empresa/cadastro-empresa.page.html"),
            styles: [__webpack_require__(/*! ./cadastro-empresa.page.scss */ "./src/app/cadastro-empresa/cadastro-empresa.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
            _empresa_service__WEBPACK_IMPORTED_MODULE_6__["EmpresaService"], _alert_service__WEBPACK_IMPORTED_MODULE_7__["AlertService"]])
    ], CadastroEmpresaPage);
    return CadastroEmpresaPage;
}());



/***/ })

}]);
//# sourceMappingURL=cadastro-empresa-cadastro-empresa-module.js.map