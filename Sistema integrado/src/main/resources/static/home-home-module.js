(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");







var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.html":
/*!*************************************!*\
  !*** ./src/app/home/home.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>\r\n        VanFácil\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <ion-card>\r\n    <ion-card-header class=\"card-header-forms-no-bottom\">\r\n      <ion-card-title>Acesse sua conta</ion-card-title>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      <form [formGroup]=\"formularioLogin\" (ngSubmit)=\"onSubmit(formularioLogin.value)\">\r\n\r\n          <ng-container *ngIf=\"errorAutenticacao\">\r\n              <div class=\"error-message\">\r\n                  <ion-icon name=\"information-circle-outline\"></ion-icon> Email e/ou senha inválido(s)!\r\n              </div>\r\n          </ng-container>\r\n\r\n        <ion-item>\r\n          <ion-label position=\"floating\">E-mail:</ion-label>\r\n          <ion-input (keydown)=\"setErroAuthFalse()\" type=\"email\" formControlName=\"login\"></ion-input>\r\n        </ion-item>\r\n\r\n        <ng-container *ngFor=\"let validation of validationMessages.login\">\r\n          <div class=\"error-message\" *ngIf=\"formularioLogin.get('login').hasError(validation.type) && (formularioLogin.get('login').dirty || formularioLogin.get('login').touched)\">\r\n            <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\r\n          </div>\r\n        </ng-container>\r\n\r\n        <ion-item>\r\n          <ion-label position=\"floating\">Senha:</ion-label>\r\n          <ion-input (keydown)=\"setErroAuthFalse()\" type=\"password\" formControlName=\"senha\"></ion-input>\r\n        </ion-item>\r\n\r\n        <ng-container *ngFor=\"let validation of validationMessages.senha\">\r\n          <div class=\"error-message\" *ngIf=\"formularioLogin.get('senha').hasError(validation.type) && (formularioLogin.get('senha').dirty || formularioLogin.get('senha').touched)\">\r\n            <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\r\n          </div>\r\n        </ng-container>\r\n\r\n        <ion-button color=\"dark\" class=\"enviar-button\" expand=\"full\" type=\"submit\" [disabled]=\"!formularioLogin.valid || !disponibilidade\">Entrar</ion-button>\r\n      </form>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-card>\r\n    <ion-card-header>\r\n      <ion-card-subtitle>Descubra!</ion-card-subtitle>\r\n      <ion-card-title>O que é o VanFácil?</ion-card-title>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      O VanFácil é a solução para o motorista e o passageiro de van! Rotas feitas de forma automática e muito mais!\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-button color=\"dark\"  routerLink=\"/cadastro-empresa\" id=\"cadastro-button\" class=\"button\" expand=\"block\">Cadastrar sua empresa</ion-button>\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".button {\n  margin-top: 13px; }\n\n#cadastro-button {\n  margin-left: 16px;\n  margin-right: 16px; }\n\n.welcome-card ion-img {\n  max-height: 35vh;\n  overflow: hidden; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXEx1aXMgRWR1YXJkb1xcR2l0XFx2YW5GYWNpbEFwcFYyL3NyY1xcYXBwXFxob21lXFxob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFnQixFQUFBOztBQUdwQjtFQUNFLGlCQUFpQjtFQUNqQixrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxnQkFBZ0I7RUFDaEIsZ0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ1dHRvbntcclxuICAgIG1hcmdpbi10b3A6IDEzcHg7XHJcbiAgfVxyXG5cclxuI2NhZGFzdHJvLWJ1dHRvbntcclxuICBtYXJnaW4tbGVmdDogMTZweDtcclxuICBtYXJnaW4tcmlnaHQ6IDE2cHg7XHJcbn1cclxuXHJcbi53ZWxjb21lLWNhcmQgaW9uLWltZyB7XHJcbiAgbWF4LWhlaWdodDogMzV2aDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _usuario_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../usuario.service */ "./src/app/usuario.service.ts");
/* harmony import */ var _usuario__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../usuario */ "./src/app/usuario.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _tipo_enum__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../tipo.enum */ "./src/app/tipo.enum.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _empresa_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../empresa.service */ "./src/app/empresa.service.ts");
/* harmony import */ var _alert_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../alert.service */ "./src/app/alert.service.ts");
/* harmony import */ var _services_passageiro_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/passageiro.service */ "./src/app/services/passageiro.service.ts");
/* harmony import */ var _auth_passageiro_passageiro_guard_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../auth/passageiro/passageiro-guard.service */ "./src/app/auth/passageiro/passageiro-guard.service.ts");
/* harmony import */ var _services_motorista_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../services/motorista.service */ "./src/app/services/motorista.service.ts");
/* harmony import */ var _auth_motorista_motorista_guard_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../auth/motorista/motorista-guard.service */ "./src/app/auth/motorista/motorista-guard.service.ts");

/* tslint:disable:no-trailing-whitespace */













var HomePage = /** @class */ (function () {
    function HomePage(formBuilder, usuarioService, router, authService, empresaService, alertService, passageiroService, passageiroAuthService, motoristaService, motoristaAuthService) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.usuarioService = usuarioService;
        this.router = router;
        this.authService = authService;
        this.empresaService = empresaService;
        this.alertService = alertService;
        this.passageiroService = passageiroService;
        this.passageiroAuthService = passageiroAuthService;
        this.motoristaService = motoristaService;
        this.motoristaAuthService = motoristaAuthService;
        this.validationMessages = {
            login: [
                { type: 'maxlength', message: 'Máximo de 100 caractéres' }
            ],
            senha: [
                { type: 'maxlength', message: 'Máximo de 50 caractéres' }
            ]
        };
        this.router.events.subscribe(function (e) {
            if (e instanceof _angular_router__WEBPACK_IMPORTED_MODULE_7__["NavigationEnd"] || _angular_router__WEBPACK_IMPORTED_MODULE_7__["NavigationStart"]) {
                _this.checkAlerts();
            }
        });
        this.disponibilidade = true;
        this.errorAutenticacao = false;
        this.formularioLogin = this.formBuilder.group({
            login: '',
            senha: ''
        });
    }
    HomePage.prototype.ngOnInit = function () {
        this.formularioLogin = this.formBuilder.group({
            login: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(100)
                ]],
            senha: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)
                ]]
        });
    };
    HomePage.prototype.checkAlerts = function () {
        if (localStorage.getItem('empresaCadastradaAlert') === 'true') {
            this.alertService.showSimpleAlert('Sucesso!', 'Empresa cadastrada!');
            localStorage.setItem('empresaCadastradaAlert', 'false');
        }
    };
    HomePage.prototype.onSubmit = function (values) {
        var _this = this;
        this.disponibilidade = false;
        var usuario = new _usuario__WEBPACK_IMPORTED_MODULE_4__["Usuario"]();
        usuario.setLogin(values.login);
        usuario.setSenha(values.senha);
        this.usuarioService.checkUsuario(usuario)
            .subscribe(function (data) { return _this.continueLogin(data); }, function (erro) { return _this.exceptionHandler(erro); });
    };
    HomePage.prototype.continueLogin = function (usuario) {
        var _this = this;
        if (usuario.tipo.toString() === _tipo_enum__WEBPACK_IMPORTED_MODULE_6__["Tipo"][_tipo_enum__WEBPACK_IMPORTED_MODULE_6__["Tipo"].EMPRESA]) {
            this.disponibilidade = true;
            this.empresaService.getEmpresaByUsuario(usuario)
                .subscribe(function (data) { return _this.salvarRedirecionarEmpresa(data); }, function (erro) { return _this.exceptionHandler(erro); });
        }
        else if (usuario.tipo.toString() === _tipo_enum__WEBPACK_IMPORTED_MODULE_6__["Tipo"][_tipo_enum__WEBPACK_IMPORTED_MODULE_6__["Tipo"].PASSAGEIRO]) {
            this.disponibilidade = true;
            this.passageiroService.getPassageiroByUsuario(usuario)
                .subscribe(function (data) { return _this.salvarRedirecionarPassageiro(data); }, function (erro) { return _this.exceptionHandler(erro); });
        }
        else {
            this.disponibilidade = true;
            this.motoristaService.getMotoristaByUsuario(usuario)
                .subscribe(function (data) { return _this.salvarRedirecionarMotorista(data); }, function (erro) { return _this.exceptionHandler(erro); });
        }
    };
    HomePage.prototype.salvarRedirecionarMotorista = function (motorista) {
        this.motoristaAuthService.login(motorista);
        this.router.navigate(['/principal-motorista']);
    };
    HomePage.prototype.salvarRedirecionarPassageiro = function (passageiro) {
        this.passageiroAuthService.login(passageiro);
        this.router.navigate(['/principal-passageiro']);
    };
    HomePage.prototype.salvarRedirecionarEmpresa = function (empresa) {
        this.authService.login(empresa);
        this.router.navigate(['/minha-empresa']);
    };
    HomePage.prototype.exceptionHandler = function (erro) {
        this.disponibilidade = true;
        if (erro.status === 403) {
            this.errorAutenticacao = true;
        }
        else {
            console.log(erro);
        }
    };
    HomePage.prototype.setErroAuthFalse = function () {
        this.errorAutenticacao = false;
    };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _usuario_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
            _empresa_service__WEBPACK_IMPORTED_MODULE_8__["EmpresaService"],
            _alert_service__WEBPACK_IMPORTED_MODULE_9__["AlertService"],
            _services_passageiro_service__WEBPACK_IMPORTED_MODULE_10__["PassageiroService"],
            _auth_passageiro_passageiro_guard_service__WEBPACK_IMPORTED_MODULE_11__["PassageiroGuardService"],
            _services_motorista_service__WEBPACK_IMPORTED_MODULE_12__["MotoristaService"],
            _auth_motorista_motorista_guard_service__WEBPACK_IMPORTED_MODULE_13__["MotoristaGuardService"]])
    ], HomePage);
    return HomePage;
}());



/***/ }),

/***/ "./src/app/usuario.service.ts":
/*!************************************!*\
  !*** ./src/app/usuario.service.ts ***!
  \************************************/
/*! exports provided: UsuarioService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioService", function() { return UsuarioService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");

/* tslint:disable:no-trailing-whitespace */



var UsuarioService = /** @class */ (function () {
    function UsuarioService(http) {
        this.http = http;
        this.baseUrl = _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"].rootUrl + 'usuario';
    }
    UsuarioService.prototype.postUsuario = function (usuario) {
        return this.http.post(this.baseUrl, usuario);
    };
    UsuarioService.prototype.checkUsuario = function (usuario) {
        return this.http.post(this.baseUrl + '/auth', usuario);
    };
    UsuarioService.prototype.getUsuario = function () {
    };
    UsuarioService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UsuarioService);
    return UsuarioService;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map