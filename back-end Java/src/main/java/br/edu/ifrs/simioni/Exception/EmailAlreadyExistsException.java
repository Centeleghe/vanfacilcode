package br.edu.ifrs.simioni.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT,
        reason = "Email já cadastrado.")
public class EmailAlreadyExistsException extends RuntimeException{
    public EmailAlreadyExistsException() {
        super("O Email já existe");
    }
}
