package br.edu.ifrs.simioni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class VanApplication {

	public static void main(String[] args) {
		SpringApplication.run(VanApplication.class, args);
	}

}
