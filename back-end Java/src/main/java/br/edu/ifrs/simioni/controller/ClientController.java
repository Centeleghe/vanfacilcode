package br.edu.ifrs.simioni.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ClientController {
    @GetMapping(value = "/app/**")
    @ResponseBody
    public String forward() {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "\n" +
                "<head>\n" +
                "  <meta charset=\"utf-8\" />\n" +
                "  <title>VanFácil</title>\n" +
                "\n" +
                "  <base href=\"/app\" />\n" +
                "\n" +
                "  <meta name=\"viewport\" content=\"viewport-fit=cover, width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\" />\n" +
                "  <meta name=\"format-detection\" content=\"telephone=no\" />\n" +
                "  <meta name=\"msapplication-tap-highlight\" content=\"no\" />\n" +
                "\n" +
                "  <link rel=\"icon\" type=\"image/png\" href=\"assets/icon/vanfacilLogo.png\" />\n" +
                "\n" +
                "  <!-- add to homescreen for ios -->\n" +
                "  <meta name=\"apple-mobile-web-app-capable\" content=\"yes\" />\n" +
                "  <meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\" />\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "  <app-root></app-root>\n" +
                "  <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyD99BijxRCyuFMxtXBFWxTBUjjMmBrORW4&libraries=places&language=pt\"></script>\n" +
                "<script type=\"text/javascript\" src=\"runtime.js\"></script><script type=\"text/javascript\" src=\"es2015-polyfills.js\" nomodule></script><script type=\"text/javascript\" src=\"polyfills.js\"></script><script type=\"text/javascript\" src=\"styles.js\"></script><script type=\"text/javascript\" src=\"vendor.js\"></script><script type=\"text/javascript\" src=\"main.js\"></script></body>\n" +
                "\n" +
                "</html>\n ";
    }
}
