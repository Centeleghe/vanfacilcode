package br.edu.ifrs.simioni.controller;

import br.edu.ifrs.simioni.model.Cronograma;
import br.edu.ifrs.simioni.model.Passageiro;
import br.edu.ifrs.simioni.model.Rota;
import br.edu.ifrs.simioni.model.Usuario;
import br.edu.ifrs.simioni.service.PassageiroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/passageiro")
public class PassageiroController {

    @Autowired
    PassageiroService passageiroService;

    @PostMapping
    public Passageiro insertPassageiro(@RequestBody Passageiro passageiro) {
        return passageiroService.postPassageiro(passageiro);
    }

    @GetMapping
    public List<Passageiro> getAllPassageiro() {
        return passageiroService.getAllPassageiro();
    }

    @GetMapping("/{id}")
    public Passageiro getOnePassageiro(@PathVariable("id") Integer id) {
        return passageiroService.getOnePassageiro(id);
    }

    @GetMapping("/rota-by-passageiro/{id}")
    public List<Rota> getPassageirosByRota(@PathVariable("id") Integer id) {
        return passageiroService.getRotaByPassageiro(id);
    }

    @GetMapping("/cronograma/{idPassageiro}/{idRota}")
    public Set<Cronograma> getCronograma(@PathVariable ("idPassageiro") Integer idPassageiro,
                                         @PathVariable ("idRota") Integer idRota){
        return passageiroService.getCronograma(idPassageiro, idRota);
    }

    @PostMapping("/byUsuario")
    public Passageiro getByUsuario(@RequestBody Usuario usuario){
        return passageiroService.getByUsuario(usuario);
    }

    @DeleteMapping(path="/{id}")
    public void deletePassageiro(@PathVariable("id") Integer id) {
        passageiroService.deletePassageiro(id);
    }

    @PostMapping("/updateCronograma/{idPassageiro}/{idRota}")
    public void updateCronograma(@PathVariable ("idPassageiro") Integer idPassageiro,
                                            @PathVariable ("idRota") Integer idRota,
                                            @RequestBody Set<Cronograma> cronogramas){
        passageiroService.updateCronograma(cronogramas, idPassageiro, idRota);
    }
}
