package br.edu.ifrs.simioni.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name="cronograma")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Cronograma {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @JsonIgnore
    @ManyToOne
    private PassageiroRota passageiroRota;

    @Column(name = "dia_semana")
    private DiaSemana diaSemana;

    @Column(name = "ida")
    private boolean ida;

    @Column(name = "volta")
    private boolean volta;
}
