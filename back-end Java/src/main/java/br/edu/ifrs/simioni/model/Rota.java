package br.edu.ifrs.simioni.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@Table(name="rota")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")

public class Rota {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    private Empresa empresa;

    @Column(name = "nome")
    private String nome;

    @Column(name = "endereco_escola")
    private String enderecoEscola;

    @Column(name = "endereco_garagem")
    private String enderecoGaragem;

    @Column(name = "turno")
    private Turno turno;

    private Double latEscola;

    private Double lngEscola;

    private Double latGaragem;

    private Double lngGaragem;

    @JsonIgnore
    @OneToMany(mappedBy = "rota", cascade = CascadeType.ALL)
    Set<PassageiroRota> passageiroRotaSet;

    @JsonIgnore
    @OneToMany(mappedBy = "rota", cascade = CascadeType.ALL)
    Set<MotoristaRota> motoristaRotaSet;

    @Override
    public String toString() {
        return "Rota{" +
                "id=" + id +
                '}';
    }
}
