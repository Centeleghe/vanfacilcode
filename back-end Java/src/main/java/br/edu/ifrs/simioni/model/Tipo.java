package br.edu.ifrs.simioni.model;

public enum Tipo {
	
	EMPRESA, PASSAGEIRO, MOTORISTA;
}
