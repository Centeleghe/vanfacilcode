package br.edu.ifrs.simioni.repository;

import br.edu.ifrs.simioni.model.MotoristaRota;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MotoristaRotaRepository extends JpaRepository<MotoristaRota, Integer> {
}
