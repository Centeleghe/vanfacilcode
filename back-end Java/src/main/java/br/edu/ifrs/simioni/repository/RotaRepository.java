package br.edu.ifrs.simioni.repository;

import br.edu.ifrs.simioni.model.Rota;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RotaRepository extends JpaRepository<Rota, Integer> {
}
