package br.edu.ifrs.simioni.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.ifrs.simioni.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer>{
    boolean existsUsuarioByLogin(String login);
    Usuario getByLogin(String login);
}
