package br.edu.ifrs.simioni.roterizador.domains;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Agente {
    private String id;
    private Coordenada coordenadaInicial;
    private Coordenada coordenadaFinal;
}
