package br.edu.ifrs.simioni.roterizador.domains;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
public class Localizacao {
    private String id;
    private Coordenada coordenada;
}
