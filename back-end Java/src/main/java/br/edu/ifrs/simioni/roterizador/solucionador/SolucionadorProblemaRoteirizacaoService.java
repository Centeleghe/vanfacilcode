package br.edu.ifrs.simioni.roterizador.solucionador;

import br.edu.ifrs.simioni.roterizador.domains.ResultadoRoteirizacao;
import br.edu.ifrs.simioni.roterizador.domains.SolicitacaoRoteirizacao;
import com.graphhopper.jsprit.core.algorithm.VehicleRoutingAlgorithm;
import com.graphhopper.jsprit.core.algorithm.box.Jsprit;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem;
import com.graphhopper.jsprit.core.problem.solution.VehicleRoutingProblemSolution;
import com.graphhopper.jsprit.core.util.Solutions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SolucionadorProblemaRoteirizacaoService {

    @Autowired
    private SolicitacaoRoteirizacaoToVrpMapper solicitacaoRoteirizacaoToVrpMapper;

    @Autowired
    private VrpToSolucaoRoteirizacaoMapper vrpToSolucaoRoteirizacaoMapper;

    public ResultadoRoteirizacao solucionar(SolicitacaoRoteirizacao solicitacaoRoteirizacao) {
        VehicleRoutingProblem vrp = solicitacaoRoteirizacaoToVrpMapper.map(solicitacaoRoteirizacao);

        VehicleRoutingAlgorithm vra = Jsprit.createAlgorithm(vrp);

        VehicleRoutingProblemSolution bestSolution = Solutions.bestOf(vra.searchSolutions());

        return vrpToSolucaoRoteirizacaoMapper.map(bestSolution);
    }
}
