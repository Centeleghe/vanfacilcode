package br.edu.ifrs.simioni.service;

import br.edu.ifrs.simioni.Exception.EmailAlreadyExistsException;
import br.edu.ifrs.simioni.model.Motorista;
import br.edu.ifrs.simioni.model.MotoristaRota;
import br.edu.ifrs.simioni.model.Rota;
import br.edu.ifrs.simioni.model.Usuario;
import br.edu.ifrs.simioni.repository.MotoristaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class MotoristaService {
    @Autowired
    private MotoristaRepository motoristaRepository;
    @Autowired
    private  UsuarioService usuarioService;

    public Motorista postMotorista(Motorista motorista) throws EmailAlreadyExistsException {

        if(motorista.getId() == null) {
            if (!usuarioService.existsUsuarioByLogin(motorista.getUsuario().getLogin())) {

                Usuario usuarioResponse = usuarioService.postUsuario(motorista.getUsuario());
                motorista.setUsuario(usuarioResponse);

                return motoristaRepository.save(motorista);
            } else {
                throw new EmailAlreadyExistsException();
            }
        } else {
            Motorista motoristaBanco = getOneMotorista(motorista.getId());
            motorista.setMotoristaRotaSet(motoristaBanco.getMotoristaRotaSet());
            return motoristaRepository.save(motorista);
        }
    }

    public Motorista getOneMotorista(Integer id) {
        return motoristaRepository.getOne(id);
    }

    public List<Motorista> getAllMotorista(){
        return motoristaRepository.findAll();
    }


    public Motorista getByUsuario(Usuario usuario) {
        Motorista passageiro = motoristaRepository.getByUsuario(usuario);
        passageiro.setUsuario(null);
        return passageiro;
    }

    public List<Rota> getRotaByMotorista(Integer id) {
        Motorista motoristaBanco = this.getOneMotorista(id);
        Set<MotoristaRota> motoristaRotaSet = motoristaBanco.getMotoristaRotaSet();
        List<Rota> rotas = new ArrayList<>();

        for (MotoristaRota motoristaRota : motoristaRotaSet) {
            rotas.add(motoristaRota.getRota());
        }
        return rotas;
    }

    public void deleteMotorista(Integer id) {
        motoristaRepository.deleteById(id);
    }
}
