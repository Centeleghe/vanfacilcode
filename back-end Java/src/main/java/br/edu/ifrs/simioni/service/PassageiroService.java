package br.edu.ifrs.simioni.service;

import br.edu.ifrs.simioni.Exception.EmailAlreadyExistsException;
import br.edu.ifrs.simioni.model.*;
import br.edu.ifrs.simioni.repository.PassageiroRepository;
import br.edu.ifrs.simioni.repository.PassageiroRotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PassageiroService {

    @Autowired
    private PassageiroRepository passageiroRepository;
    @Autowired
    private  UsuarioService usuarioService;
    @Autowired
    private PassageiroRotaRepository passageiroRotaRepository;

    public Passageiro postPassageiro(Passageiro passageiro) throws EmailAlreadyExistsException {

        if(passageiro.getId() == null) {
            if (!usuarioService.existsUsuarioByLogin(passageiro.getUsuario().getLogin())) {

                Usuario usuarioResponse = usuarioService.postUsuario(passageiro.getUsuario());
                passageiro.setUsuario(usuarioResponse);

                return passageiroRepository.save(passageiro);
            } else {
                throw new EmailAlreadyExistsException();
            }
        } else {
            Passageiro passageiroBanco = this.getOnePassageiro(passageiro.getId());
            passageiro.setPassageiroRotaSet(passageiroBanco.getPassageiroRotaSet());
            return passageiroRepository.save(passageiro);
        }
    }

    public Passageiro getOnePassageiro(Integer id) {
        return passageiroRepository.getOne(id);
    }

    public List<Passageiro> getAllPassageiro(){
        return passageiroRepository.findAll();
    }

    public void deletePassageiro(Integer id) {
        passageiroRepository.deleteById(id);
    }

    public Passageiro getByUsuario(Usuario usuario) {
        Passageiro passageiro = passageiroRepository.getByUsuario(usuario);
        passageiro.setUsuario(null);
        return passageiro;
    }

    public List<Rota> getRotaByPassageiro(Integer id) {
        Passageiro passageiroBanco = this.getOnePassageiro(id);
        Set<PassageiroRota> passageiroRotaSet = passageiroBanco.getPassageiroRotaSet();
        List<Rota> rotas = new ArrayList<>();

        for (PassageiroRota passageiroRota : passageiroRotaSet) {
            rotas.add(passageiroRota.getRota());
        }
        return rotas;
    }

    public Set<Cronograma> getCronograma(Integer idPassageiro, Integer idRota) {
        Passageiro passageiroBanco = this.getOnePassageiro(idPassageiro);
        Set<PassageiroRota> passageiroRotaSet = passageiroBanco.getPassageiroRotaSet();

        Set<Cronograma> cronogramas = new HashSet<>();
        for(PassageiroRota passageiroRota: passageiroRotaSet) {
            if(passageiroRota.getRota().getId().equals(idRota)) {
                cronogramas = passageiroRota.getCronogramas();
            }
        }
        return cronogramas;
    }

    public void updateCronograma(Set<Cronograma> cronogramas, Integer idPassageiro, Integer idRota) {
        Passageiro passageiroBanco = this.getOnePassageiro(idPassageiro);
        Set<PassageiroRota> passageiroRotaSet = passageiroBanco.getPassageiroRotaSet();

        for(PassageiroRota passageiroRota: passageiroRotaSet) {
            if(passageiroRota.getRota().getId().equals(idRota)) {
                for(Cronograma cronograma: cronogramas){
                    cronograma.setPassageiroRota(passageiroRota);
                }
                passageiroRota.setCronogramas(cronogramas);
                passageiroRotaRepository.save(passageiroRota);
            }
        }
    }
}
