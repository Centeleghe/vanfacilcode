package br.edu.ifrs.simioni.service;

import br.edu.ifrs.simioni.model.*;
import br.edu.ifrs.simioni.repository.MotoristaRotaRepository;
import br.edu.ifrs.simioni.repository.PassageiroRotaRepository;
import br.edu.ifrs.simioni.repository.RotaRepository;
import br.edu.ifrs.simioni.roterizador.domains.*;
import br.edu.ifrs.simioni.roterizador.solucionador.SolucionadorProblemaRoteirizacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RotaService {
    @Autowired
    RotaRepository rotaRepository;
    @Autowired
    EmpresaService empresaService;
    @Autowired
    PassageiroRotaRepository passageiroRotaRepository;
    @Autowired
    SolucionadorProblemaRoteirizacaoService solucionadorProblemaRoteirizacaoService;
    @Autowired
    PassageiroService passageiroService;
    @Autowired
    MotoristaRotaRepository motoristaRotaRepository;
    @Autowired
    MotoristaService motoristaService;

    public Rota postRota(Rota rota){
        return rotaRepository.save(rota);
    }

    public Rota getOneRota(Integer id) {
        return rotaRepository.getOne(id);
    }

    public List<Rota> getAllRota(){
        return rotaRepository.findAll();
    }

    public void deleteRota(Integer id) {
        rotaRepository.deleteById(id);
    }

    //MOTORISTA
    public void addMotoristaToRota(Integer id, Motorista motorista) {
        Rota rotaBanco = this.getOneRota(id);

        MotoristaRota motoristaRota = new MotoristaRota();
        motoristaRota.setMotorista(motorista);
        motoristaRota.setRota(rotaBanco);

        motoristaRotaRepository.save(motoristaRota);
    }

    public List<Motorista> getMotoristasByRota(Integer id) {
        Rota rotaBanco = this.getOneRota(id);
        Set<MotoristaRota> motoristaRotaSet = rotaBanco.getMotoristaRotaSet();
        List<Motorista> motoristas = new ArrayList<>();

        for (MotoristaRota motoristaRota :motoristaRotaSet) {
            motoristas.add(motoristaRota.getMotorista());
        }
        return motoristas;
    }

    public List<Motorista> getMotoristasForaRota(Integer id) {
        Rota rotaBanco = this.getOneRota(id);
        boolean controller;

        List<Motorista> motoristasRota = this.getMotoristasByRota(id);
        Set<Motorista> motoristasTotais = empresaService.getMotoristasEmpresa(rotaBanco.getEmpresa().getId());
        List<Motorista> motoristasSobra = new ArrayList<>();

        for (Motorista motorista: motoristasTotais) {
            controller = true;
            for (Motorista motoristaRota: motoristasRota) {
                if(motorista.getId().equals(motoristaRota.getId())){
                    controller = false;
                }
            }
            if (controller) {
                motoristasSobra.add(motorista);
            }
        }
        return motoristasSobra;
    }

    public void deleteMotoristaFromRota(Integer id, Motorista motorista) {
        Rota rotaBanco = this.getOneRota(id);

        Set<MotoristaRota> motoristaRotas = rotaBanco.getMotoristaRotaSet();

        Motorista motoristaBanco = motoristaService.getOneMotorista(motorista.getId());

        for (MotoristaRota motoristaRota: motoristaRotas) {
            if(motoristaRota.getMotorista().getId().equals( motoristaBanco.getId())){
                motoristaBanco.getMotoristaRotaSet().remove(motoristaRota);
                motoristaService.postMotorista(motoristaBanco);
            }
        }
    }

    //PASSAGEIRO
    public List<Passageiro> getPassageirosByRota(Integer id) {
        Rota rotaBanco = this.getOneRota(id);
        Set<PassageiroRota> passageiroRotaSet = rotaBanco.getPassageiroRotaSet();
        List<Passageiro> passageiros = new ArrayList<>();

        for (PassageiroRota passageiroRota:passageiroRotaSet) {
            passageiros.add(passageiroRota.getPassageiro());
        }
        return passageiros;
    }

    public List<Passageiro> getPassageirosForaRota(Integer id) {
        Rota rotaBanco = this.getOneRota(id);
        boolean controller;

        List<Passageiro> passageirosRota = this.getPassageirosByRota(id);
        Set<Passageiro> passageirosTotais = empresaService.getPassageirosEmpresa(rotaBanco.getEmpresa().getId());
        List<Passageiro> passageirosSobra = new ArrayList<>();

        for (Passageiro passageiro:passageirosTotais) {
            controller = true;
            for (Passageiro passageiroRota:passageirosRota) {
                if(passageiro.getId().equals(passageiroRota.getId())){
                    controller = false;
                }
            }
            if (controller) {
                passageirosSobra.add(passageiro);
            }
        }
        return passageirosSobra;
    }

    public void addPassageiroToRota(Integer id, Passageiro passageiro) {
        Rota rotaBanco = this.getOneRota(id);

        PassageiroRota passageiroRota = new PassageiroRota();
        passageiroRota.setPassageiro(passageiro);
        passageiroRota.setRota(rotaBanco);

        passageiroRota.setCronogramas(new HashSet<>());
        PassageiroRota passageiroRotaSalvo = passageiroRotaRepository.save(passageiroRota);

        for (int i=0; i<5; i++) {
            Cronograma cronograma = new Cronograma();
            if (i == 0){
                cronograma.setDiaSemana(DiaSemana.SEGUNDA);
            }else if (i == 1) {
                cronograma.setDiaSemana(DiaSemana.TERCA);
            } else if (i == 2) {
                cronograma.setDiaSemana(DiaSemana.QUARTA);
            } else if (i == 3) {
                cronograma.setDiaSemana(DiaSemana.QUINTA);
            } else {
                cronograma.setDiaSemana(DiaSemana.SEXTA);
            }
            cronograma.setIda(true);
            cronograma.setVolta(true);

            cronograma.setPassageiroRota(passageiroRotaSalvo);
            passageiroRotaSalvo.getCronogramas().add(cronograma);
        }
        passageiroRotaRepository.save(passageiroRota);
    }

    public void deletePassageiroFromRota(Integer id, Passageiro passageiro) {
        Rota rotaBanco = this.getOneRota(id);

        Set<PassageiroRota> passageiroRotas = rotaBanco.getPassageiroRotaSet();

        Passageiro passageiroBanco = passageiroService.getOnePassageiro(passageiro.getId());

        for (PassageiroRota passageiroRota: passageiroRotas) {
            if(passageiroRota.getPassageiro().getId().equals( passageiroBanco.getId())){
                passageiroBanco.getPassageiroRotaSet().remove(passageiroRota);
                passageiroService.postPassageiro(passageiroBanco);
            }
        }
    }

    public List<Passageiro> getPassageirosOrdenados(Integer idRota,
                                                    TipoViagem tipo,
                                                    DiaSemana diaSemana,
                                                    Coordenada posicaoVan) {
        Rota rotaBanco = this.getOneRota(idRota);

        List<Visita> visitas = generateVisitas(rotaBanco, diaSemana, tipo);

        if(!(visitas.isEmpty())) {
            SolicitacaoRoteirizacao solicitacaoRoteirizacao = SolicitacaoRoteirizacao.builder()
                    .agente(generateAgente(rotaBanco, posicaoVan, tipo))
                    .visitas(visitas)
                    .rota(rotaBanco)
                    .build();

            ResultadoRoteirizacao resultadoRoteirizacao = solucionadorProblemaRoteirizacaoService.solucionar(solicitacaoRoteirizacao);

            return resultadoRoteirizacao
                    .getAtividades()
                    .stream()
                    .map(atividade -> atividade.getVisita().getPassageiro())
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    private List<Visita> generateVisitas(Rota rota, DiaSemana diaSemana, TipoViagem tipo) {
        List<Passageiro> passageiros = getPassageirosViagem(rota, diaSemana, tipo);

        return passageiros.stream()
                .map(passageiro -> generateVisita(passageiro))
                .collect(Collectors.toList());
    }

    private Visita generateVisita(Passageiro passageiro) {
        return Visita.forPassageiro(passageiro);
    }

    private List<Passageiro> getPassageirosViagem(Rota rota, DiaSemana diaSemana, TipoViagem tipo) {
        List<PassageiroRota> passageiroRotas = passageiroRotaRepository.getAllByRota(rota);

        List<Passageiro> passageiros = new ArrayList<>();

        for (PassageiroRota passageiroRota : passageiroRotas) {
            for (Cronograma cronograma: passageiroRota.getCronogramas()) {
                if (cronograma.getDiaSemana() == diaSemana) {
                   if (tipo == TipoViagem.IDA) {
                      if (cronograma.isIda()) {
                         passageiros.add(passageiroRota.getPassageiro());
                      }
                   } else {
                       if (cronograma.isVolta()) {
                           passageiros.add(passageiroRota.getPassageiro());
                       }
                   }
                }
            }
        }

        return passageiros;
    }

    private Agente generateAgente(Rota rota, Coordenada posicaoVan, TipoViagem tipoViagem) {
        return Agente.builder()
                .id("1")
                .coordenadaInicial(posicaoVan)
                .coordenadaFinal(getCoordenadaFinalAgente(rota, tipoViagem))
                .build();
    }

    private Coordenada getCoordenadaFinalAgente(Rota rota, TipoViagem tipoViagem) {
        if(tipoViagem == TipoViagem.IDA) {
            return new Coordenada(rota.getLatEscola(), rota.getLngEscola());
        } else {
            return new Coordenada(rota.getLatGaragem(), rota.getLngGaragem());
        }
    }
}
